package com.steafan.cits.questionBank.controller;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.questionBank.BO.UserQuestionBankAnswerBO;
import com.steafan.cits.questionBank.service.IQuestionBankService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @author steafan
 * @Title: QuestionBankController
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/914:20
 */
@Slf4j
@Controller
@RequestMapping(value = "/questionBank/")
public class QuestionBankController {


    @Autowired
    private IQuestionBankService questionBankService;

    Channel channel = null;
    private static final String LOGIN_USER_SESSION_QUEUE = "userLoginSession";


    /**
     * 根据题目类型获取题目列表
     *
     * @param questionType
     */
    @RequestMapping(value = "query_question_bank_list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据题目类型获取题目列表", tags = {"题库"})
    public void queryQuestionBankList(@ApiParam(value = "questionType-题目类型", required = true) int questionType){
        questionBankService.queryQuestionBankList(questionType);
    }

    /**
     * 匹配用户答题结果
     *
     * @param session
     * @param userQuestionBankAnswerBO
     * @return
     */
    @RequestMapping(value = "match_user_answer.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "匹配用户答题结果", tags = {"题库"})
    public CommonResponse<String> matchUserAnswer(
            @ApiParam("用户登录session") HttpSession session,
            @ApiParam("用户答题记录BO-userQuestionBankAnswerBO") @RequestBody UserQuestionBankAnswerBO userQuestionBankAnswerBO){
        return questionBankService.matchUserAnswer(session, userQuestionBankAnswerBO);
    }

    @RequestMapping(value = "rabbit_mq_interprocess_test.do", method = RequestMethod.POST)
    @ResponseBody
    public String rabbitMq(){
        ConnectionFactory connectionFactory = new ConnectionFactory();
        try (Connection connection = connectionFactory.newConnection()) {
            final String[] message = {""};
            channel = connection.createChannel();
            channel.queueDeclare(LOGIN_USER_SESSION_QUEUE, false, false, false, null);

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                message[0] = new String(delivery.getBody(), "UTF-8");
//                log.info("receiving user login session message {}", message);
            };
            String str = channel.basicConsume(LOGIN_USER_SESSION_QUEUE, true, deliverCallback, consumerTag -> {});
            log.info("mq receive content {}", message[0]);
        } catch (Exception e){
            log.error(e + "");
        }
        return null;
    }
}
