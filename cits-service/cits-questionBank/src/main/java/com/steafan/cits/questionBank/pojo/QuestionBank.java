package com.steafan.cits.questionBank.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: QuestionBank
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/915:03
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("cits_question_book")
public class QuestionBank implements Serializable {
    private int id;
    private String questionTitle;
    private String answerOne;
    private String answerTwo;
    private String answerThree;
    private String answerFour;
    private String questionType;
    private String simpleQuestionAnswer;
    private String paperQuestionAnswer;
    private String quantityChoiceCorrectAnswerCtr;
    private String createTime;
    private String updateTime;
}
