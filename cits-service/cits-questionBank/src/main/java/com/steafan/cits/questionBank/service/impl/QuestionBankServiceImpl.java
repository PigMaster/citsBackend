package com.steafan.cits.questionBank.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.steafan.cits.common.commons.CommonConst;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.common.config.redis.RedisConfig;
import com.steafan.cits.common.config.redis.RedisKeyConfig;
import com.steafan.cits.common.utils.TimeUnit;
import com.steafan.cits.questionBank.BO.UserQuestionBankAnswerBO;
import com.steafan.cits.questionBank.dao.QuestionBankMapper;
import com.steafan.cits.questionBank.pojo.QuestionBank;
import com.steafan.cits.questionBank.service.IQuestionBankService;
import com.steafan.cits.service.user.dao.UserQuestionBankMapper;
import com.steafan.cits.service.user.pojo.User;
import com.steafan.cits.service.user.pojo.UserQuestionBank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author steafan
 * @Title: QuestionBankServiceImpl
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/915:03
 */
@Slf4j
@Service("questionBankService")
public class QuestionBankServiceImpl implements IQuestionBankService {

    @Autowired
    private QuestionBankMapper questionBankMapper;
    @Autowired
    private UserQuestionBankMapper userQuestionBankMapper;

    /**
     * 题库中题目类型
     */
    private final int singleChoice = CommonConst.QUESTION_TYPE.SINGLE_CHOICE;
    private final int quantityChoice = CommonConst.QUESTION_TYPE.QUANTITY_CHOICES;
    private final int simpleQuestion = CommonConst.QUESTION_TYPE.SIMPLE_QUESTION;
    private final int paperQuestion = CommonConst.QUESTION_TYPE.PAPER_QUESTION;

    /**
     * 根据题目类型获取题库题目数据
     *
     * @param questionType
     */
    @Override
    public void queryQuestionBankList(int questionType) {
        switch (questionType) {
            case singleChoice:
                getSingleChoice();
                break;
            case quantityChoice:
                getQuantityChoice();
                break;
            case simpleQuestion:
                getSimpleQuestion();
                break;
            case paperQuestion:
                getPaperQuestion();
                break;
            default:
                break;
        }
    }

    /**
     * 匹配用户答题结果
     *
     * @param session
     * @param userQuestionBankAnswerBO
     * @return
     */
    @Override
    public CommonResponse<String> matchUserAnswer(HttpSession session, @RequestBody UserQuestionBankAnswerBO userQuestionBankAnswerBO) {
        User loginUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        if (loginUser == null) {
            return CommonResponse.errorResponse("您还没有登录，不能作答");
        }
        // 将答题信息录入用户题库表
        UserQuestionBank userQuestionBank = new UserQuestionBank();
        userQuestionBank.setUsername(loginUser.getUsername());
        userQuestionBank.setQuestionId(userQuestionBankAnswerBO.getQuestionId());
        userQuestionBank.setQuestionType(userQuestionBankAnswerBO.getQuestionType());
        // 判断是否已经录入过该题目
        QueryWrapper<UserQuestionBank> userQuestionBankQueryWrapper = new QueryWrapper<>();
        userQuestionBankQueryWrapper.eq("question_id", userQuestionBank.getQuestionId());
        userQuestionBankQueryWrapper.eq("question_type", userQuestionBank.getQuestionType());
        int alreadyCount = userQuestionBankMapper.selectCount(userQuestionBankQueryWrapper);
        if (alreadyCount > 0) {
            return CommonResponse.errorResponse("您已答过该题目");
        }
        // 没有录入：入库并存入redis
        int resultCount = userQuestionBankMapper.insert(userQuestionBank);
        Jedis jedis = RedisConfig.getJedis();
        jedis.sadd("userQuestionBankKey", JSON.toJSONString(userQuestionBank));
        if (resultCount > 0){
            // 录入成功，匹配用户答题结果
            switch (userQuestionBankAnswerBO.getQuestionType()) {
                case singleChoice:
                    return matchUserSingleChoice(userQuestionBankAnswerBO);
                case quantityChoice:
                    return matchUserQuantityChoice(userQuestionBankAnswerBO);
                case simpleQuestion:
                    return matchUserSimpleQuestionAnswer(userQuestionBankAnswerBO);
                case paperQuestion:
                    return matchUserPaperQuestionAnswer(userQuestionBankAnswerBO);
                default:
                    break;
            }
        }
        return CommonResponse.errorResponse("录入用户题库数据失败，请联系系统管理员");
    }

    /**
     * 单选题答案匹配
     *
     * @param userQuestionBankAnswerBO
     * @return
     */
    private CommonResponse<String> matchUserSingleChoice(UserQuestionBankAnswerBO userQuestionBankAnswerBO) {
        QueryWrapper<QuestionBank> singleChoiceWrapper = new QueryWrapper<>();
        singleChoiceWrapper.eq("id", userQuestionBankAnswerBO.getQuestionId());
        singleChoiceWrapper.eq("question_type", userQuestionBankAnswerBO.getQuestionType());
        singleChoiceWrapper.and(
                Wrapper -> Wrapper
                        .eq("answer_one", userQuestionBankAnswerBO.getUserSingleChoiceAnswer()).or()
                        .eq("answer_two", userQuestionBankAnswerBO.getUserSingleChoiceAnswer()).or()
                        .eq("answer_three", userQuestionBankAnswerBO.getUserSingleChoiceAnswer()).or()
                        .eq("answer_four", userQuestionBankAnswerBO.getUserSingleChoiceAnswer())
        );
        List<QuestionBank> singleChoiceMatchResultList = questionBankMapper.selectList(singleChoiceWrapper);
        if (singleChoiceMatchResultList.size() > 0){
            return CommonResponse.successResponse("恭喜回答正确！");
        }
        return CommonResponse.errorResponse("很遗憾，回答错误，请再想想哦！~~");
    }

    /**
     * 多选题答案选项匹配
     *
     * @param userQuestionBankAnswerBO
     * @return
     */
    private CommonResponse<String> matchUserQuantityChoice(UserQuestionBankAnswerBO userQuestionBankAnswerBO){
        QueryWrapper<QuestionBank> quantityChoiceWrapper = new QueryWrapper<>();
        quantityChoiceWrapper.eq("id", userQuestionBankAnswerBO.getQuestionId());
        quantityChoiceWrapper.eq("question_type", userQuestionBankAnswerBO.getQuestionType());
        quantityChoiceWrapper.eq("quantity_choice_correct_answer_ctr", userQuestionBankAnswerBO.getUserQuantityChoiceAnswer());
        int resultCount = questionBankMapper.selectCount(quantityChoiceWrapper);
        if (resultCount > 0){
            return CommonResponse.successResponse("恭喜回答正确！");
        }
        return CommonResponse.errorResponse("很遗憾，回答错误，请再想想哦！~~");
    }

    /**
     * 简答题答案模糊匹配
     *
     * @param userQuestionBankAnswerBO
     * @return
     */
    private CommonResponse<String> matchUserSimpleQuestionAnswer(UserQuestionBankAnswerBO userQuestionBankAnswerBO){
        QueryWrapper<QuestionBank> simpleQuestionWrapper = new QueryWrapper<>();
        simpleQuestionWrapper.eq("id", userQuestionBankAnswerBO.getQuestionId());
        simpleQuestionWrapper.eq("question_type", userQuestionBankAnswerBO.getQuestionType());
        simpleQuestionWrapper.and(
                Wrapper -> Wrapper
                        .like("simple_question_answer", "%" + userQuestionBankAnswerBO.getUserSimpleQuestionAnswer() + "%"));
        int resultCount = questionBankMapper.selectCount(simpleQuestionWrapper);
        if (resultCount > 0){
            return CommonResponse.successResponse("恭喜回答正确！");
        }
        return CommonResponse.errorResponse("很遗憾，回答错误，请再想想哦！~~");
    }

    /**
     * 论问题答案模糊匹配
     *
     * @param userQuestionBankAnswerBO
     * @return
     */
    private CommonResponse<String> matchUserPaperQuestionAnswer(UserQuestionBankAnswerBO userQuestionBankAnswerBO){
        QueryWrapper<QuestionBank> paperQuestionWrapper = new QueryWrapper<>();
        paperQuestionWrapper.eq("id", userQuestionBankAnswerBO.getQuestionId());
        paperQuestionWrapper.eq("question_type", userQuestionBankAnswerBO.getQuestionType());
        paperQuestionWrapper.and(
                Wrapper -> Wrapper.like("paper_question_answer", "%" + userQuestionBankAnswerBO.getUserPaperQuestionAnswer() + "%")
        );
        int resultCount = questionBankMapper.selectCount(paperQuestionWrapper);
        if (resultCount > 0){
            return CommonResponse.successResponse("恭喜回答正确！");
        }
        return CommonResponse.errorResponse("很遗憾，回答错误，请再想想哦！~~");
    }

    private CommonResponse<List<QuestionBank>> getSingleChoice() {
        Jedis singleChoiceJedis = RedisConfig.getJedis();
        QueryWrapper<QuestionBank> singleChoiceWrapper = new QueryWrapper<>();
        String singleChoiceListCache = singleChoiceJedis.get(RedisKeyConfig.SINGEL_CHOICE_QUESTION_KEY);
        List<QuestionBank> questionBankListFromRedis = new ArrayList<>();
        if (singleChoiceListCache == null || (" ").equals(singleChoiceListCache)) {
            singleChoiceWrapper.eq("question_type", singleChoice);
            List<QuestionBank> singleChoiceList = questionBankMapper.selectList(singleChoiceWrapper);
            if (singleChoiceList.size() > 0) {
                String singleChoiceStr = JSON.toJSONString(singleChoiceList);
                singleChoiceJedis.set(RedisKeyConfig.SINGEL_CHOICE_QUESTION_KEY, singleChoiceStr);
                questionBankListFromRedis =
                        JSON.parseArray(singleChoiceJedis.get(RedisKeyConfig.SINGEL_CHOICE_QUESTION_KEY), QuestionBank.class);
                return CommonResponse.successResponse(questionBankListFromRedis, questionBankListFromRedis.size());
            }
            return CommonResponse.errorResponse("系统中还没有任何题目数据");
        }
        questionBankListFromRedis = JSON.parseArray(singleChoiceListCache, QuestionBank.class);
        return CommonResponse.successResponse(questionBankListFromRedis, questionBankListFromRedis.size());
    }

    private CommonResponse<List<QuestionBank>> getQuantityChoice() {
        Jedis quantityChoiceJedis = RedisConfig.getJedis();
        QueryWrapper<QuestionBank> quantityChoiceWrapper = new QueryWrapper<>();
        String quantityChoiceListCache = quantityChoiceJedis.get(RedisKeyConfig.QUANTITY_CHOICE_QUESTION_KEY);
        List<QuestionBank> questionBankListFromRedis = new ArrayList<>();
        if (quantityChoiceListCache == null || (" ").equals(quantityChoiceListCache)) {
            quantityChoiceWrapper.eq("question_type", quantityChoice);
            questionBankListFromRedis = questionBankMapper.selectList(quantityChoiceWrapper);
            if (questionBankListFromRedis.size() > 0) {
                String quantityChoiceStr = JSON.toJSONString(questionBankListFromRedis);
                quantityChoiceJedis.set(RedisKeyConfig.QUANTITY_CHOICE_QUESTION_KEY, quantityChoiceStr);
                questionBankListFromRedis =
                        JSON.parseArray(quantityChoiceJedis.get(RedisKeyConfig.QUANTITY_CHOICE_QUESTION_KEY), QuestionBank.class);
                return CommonResponse.successResponse(questionBankListFromRedis, questionBankListFromRedis.size());
            }
            return CommonResponse.errorResponse("系统中还没有任何题目数据");
        }
        questionBankListFromRedis = JSON.parseArray(quantityChoiceListCache, QuestionBank.class);
        return CommonResponse.successResponse(questionBankListFromRedis, questionBankListFromRedis.size());
    }

    private CommonResponse<List<QuestionBank>> getSimpleQuestion() {
        Jedis simpleQuestionJedis = RedisConfig.getJedis();
        QueryWrapper<QuestionBank> simpleQuestionWrapper = new QueryWrapper<>();
        String simpleQuestionListCache = simpleQuestionJedis.get(RedisKeyConfig.SIMPLE_QUESTION_KEY);
        List<QuestionBank> questionBankListFromRedis = new ArrayList<>();
        if (simpleQuestionListCache == null || (" ").equals(simpleQuestionListCache)) {
            simpleQuestionWrapper.eq("question_type", simpleQuestion);
            questionBankListFromRedis = questionBankMapper.selectList(simpleQuestionWrapper);
            if (questionBankListFromRedis.size() > 0) {
                String simpleQuestionStr = JSON.toJSONString(questionBankListFromRedis);
                simpleQuestionJedis.set(RedisKeyConfig.SIMPLE_QUESTION_KEY, simpleQuestionStr);
                questionBankListFromRedis =
                        JSON.parseArray(simpleQuestionJedis.get(RedisKeyConfig.SIMPLE_QUESTION_KEY), QuestionBank.class);
                return CommonResponse.successResponse(questionBankListFromRedis, questionBankListFromRedis.size());
            }
            return CommonResponse.errorResponse("系统中还没有任何题目数据");
        }
        questionBankListFromRedis = JSON.parseArray(simpleQuestionListCache, QuestionBank.class);
        return CommonResponse.successResponse(questionBankListFromRedis, questionBankListFromRedis.size());
    }

    private CommonResponse<List<QuestionBank>> getPaperQuestion() {
        Jedis paperQuestionJedis = RedisConfig.getJedis();
        QueryWrapper<QuestionBank> paperQuestionJedisWrapper = new QueryWrapper<>();
        String paperQuestionListCache = paperQuestionJedis.get(RedisKeyConfig.PAPER_QUESTION_KEY);
        List<QuestionBank> questionBankListFromRedis = new ArrayList<>();
        if (paperQuestionListCache == null || (" ").equals(paperQuestionListCache)) {
            paperQuestionJedisWrapper.eq("question_type", paperQuestion);
            questionBankListFromRedis = questionBankMapper.selectList(paperQuestionJedisWrapper);
            if (questionBankListFromRedis.size() > 0) {
                String paperQuestionStr = JSON.toJSONString(questionBankListFromRedis);
                paperQuestionJedis.set(RedisKeyConfig.PAPER_QUESTION_KEY, paperQuestionStr);
                questionBankListFromRedis =
                        JSON.parseArray(paperQuestionJedis.get(RedisKeyConfig.PAPER_QUESTION_KEY), QuestionBank.class);
                return CommonResponse.successResponse(questionBankListFromRedis, questionBankListFromRedis.size());
            }
            return CommonResponse.errorResponse("系统中还没有任何题目数据");
        }
        questionBankListFromRedis = JSON.parseArray(paperQuestionListCache, QuestionBank.class);
        return CommonResponse.successResponse(questionBankListFromRedis, questionBankListFromRedis.size());
    }
}
