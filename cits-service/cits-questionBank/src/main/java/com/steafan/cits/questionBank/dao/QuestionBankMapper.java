package com.steafan.cits.questionBank.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.steafan.cits.questionBank.pojo.QuestionBank;

/**
 * @author steafan
 * @Title: QuestionBankMapper
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/915:07
 */
public interface QuestionBankMapper extends BaseMapper<QuestionBank> {


}
