package com.steafan.cits.questionBank;

import com.steafan.cits.service.user.CITSUserApplication;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author steafan
 * @Title: QuestionBankApplication
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/908:24
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan({"com.steafan.cits.questionBank.dao", "com.steafan.cits.service.user.dao"})
public class CITSQuestionBankApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(CITSUserApplication.class).run(args);
    }
}
