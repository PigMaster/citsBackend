package com.steafan.cits.questionBank.BO;

import lombok.*;

/**
 * @author steafan
 * @Title: UserQuestionBankAnswerBO
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/1108:18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserQuestionBankAnswerBO {
    private Integer questionId;
    private Integer questionType;
    private String userSingleChoiceAnswer;
    private String userQuantityChoiceAnswer;
    private String userSimpleQuestionAnswer;
    private String userPaperQuestionAnswer;
}
