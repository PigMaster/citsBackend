package com.steafan.cits.questionBank.service;

import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.questionBank.BO.UserQuestionBankAnswerBO;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpSession;

/**
 * @author steafan
 * @Title: IQuestionBankService
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/915:02
 */
public interface IQuestionBankService {

    void queryQuestionBankList(int questionType);

    CommonResponse<String> matchUserAnswer(HttpSession session, @RequestBody UserQuestionBankAnswerBO userQuestionBankAnswerBO);

}
