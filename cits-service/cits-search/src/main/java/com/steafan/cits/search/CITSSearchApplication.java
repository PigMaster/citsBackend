package com.steafan.cits.search;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author steafan
 * @Title: SearchApplication
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/908:29
 */
@SpringBootApplication
@EnableDiscoveryClient
public class CITSSearchApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(CITSSearchApplication.class).run(args);
    }
}
