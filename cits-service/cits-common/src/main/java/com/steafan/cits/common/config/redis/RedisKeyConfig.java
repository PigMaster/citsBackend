package com.steafan.cits.common.config.redis;

import com.steafan.cits.common.utils.TimeUnit;

/**
 * @author steafan
 * @Title: RedisKeyConfig
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/915:54
 */
public class RedisKeyConfig {
    /**
     * 题库相关key
     *
     */
    public static final String SINGEL_CHOICE_QUESTION_KEY = "singleChoice*" + TimeUnit.currentTimeFormat().replaceAll(" ", "");
    public static final String QUANTITY_CHOICE_QUESTION_KEY = "quantityChoice*" + TimeUnit.currentTimeFormat().replaceAll(" ", "");
    public static final String SIMPLE_QUESTION_KEY = "simpleQuestion*" + TimeUnit.currentTimeFormat().replaceAll(" ", "");
    public static final String PAPER_QUESTION_KEY = "paperQuestion*" + TimeUnit.currentTimeFormat().replaceAll(" ", "");
    /**
     * 题库模糊匹配key相关key
     *
     */
    public static final String SINGLE_CHOICE_MATCH_ANSWER_KEY = "singleChoice*";
    public static final String QUANTITY_CHOICE_MATCH_ANSWER_KEY = "quantityChoice*";
    public static final String SIMPLE_QUESTION_MATCH_ANSWER_KEY = "simpleQuestion*";
    public static final String PAPER_QUESTION_MATCH_ANSWER_KEY = "paperQuestion*";

    public static final String USER_QUESTION_KEY = "userQuestion*" + TimeUnit.currentTimeFormat().replaceAll(" ", "");
    public static final String USER_QUESTION_MATCH_KEY = "userQuestion*";
}
