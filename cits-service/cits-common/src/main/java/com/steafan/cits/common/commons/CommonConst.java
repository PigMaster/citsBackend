package com.steafan.cits.common.commons;

/**
 * @author steafan
 * @Title: CommonConst
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/1017:37
 */
public class CommonConst {

    /**
     * 当前登录用户
     *
     */
    public static final String CURRENT_USER = "loginUser";


    /**
     * 文件类型
     *
     */
    public interface FILE_TYPE {
       int PDF_FILE = 0;
       int WORD_FILE = 1;
       int EXCEL_FILE = 2;
       int PPT_FILE = 3;
       int VIDEO_TYPE = 4;
    }

    /**
     * 题库题目类型
     *
     */
    public interface QUESTION_TYPE {
        int SINGLE_CHOICE = 0;
        int QUANTITY_CHOICES = 1;
        int SIMPLE_QUESTION = 2;
        int PAPER_QUESTION = 3;
    }

    public interface USER_ROLE {
        int STUDENT = 0;
        int TEACHER = 1;
    }
}
