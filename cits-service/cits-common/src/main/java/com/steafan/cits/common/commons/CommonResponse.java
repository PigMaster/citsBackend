package com.steafan.cits.common.commons;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: CommonResponse
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/715:02
 */
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonResponse<T> implements Serializable {
    private int code;
    private String msg;
    private T data;
    private int count;

    public int getCode(){
        return code;
    }
    public String getMsg(){
        return msg;
    }
    public T getData(){
        return data;
    }
    public int getCount(){
        return count;
    }

    /**
     * 私有构造方法封装
     *
     * @param code
     */
    private CommonResponse(int code){
        this.code = code;
    }
    private CommonResponse(int code, String msg){
        this.code = code;
        this.msg = msg;
    }
    private CommonResponse(int code, String msg, T data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    private CommonResponse(int code, String msg, T data, int count){
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count;
    }
    private CommonResponse(int code, T data){
        this.code = code;
        this.data = data;
    }
    private CommonResponse(int code, T data, int count){
        this.code = code;
        this.data = data;
        this.count = count;
    }

    public CommonResponse(String msg){
        this.msg = msg;
    }

    /**
     * 响应成功方法封装
     *
     * @param <T>
     * @return
     */
    public static <T> CommonResponse<T> successResponse(){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDesc());
    }
    public static <T> CommonResponse<T> successResponse(String msg){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), msg);
    }
    public static <T> CommonResponse<T> successResponse(T data){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), data);
    }
    public static <T> CommonResponse<T> successResponse(String msg, T data){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), msg, data);
    }
    public static <T> CommonResponse<T> successResponse(int code, String msg, T data){
        return new CommonResponse<>(code, msg, data);
    }
    public static <T> CommonResponse<T> successResponse(int code, T data){
        return new CommonResponse<>(code, ResponseCode.SUCCESS.getDesc(), data);
    }
    public static <T> CommonResponse<T> successResponse(T data, int count){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDesc(), data, count);
    }

    /**
     * 响应失败方法封装
     *
     * @param <T>
     * @return
     */
    public static <T> CommonResponse<T> errorResponse(){
        return new CommonResponse<>(ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getDesc());
    }
    public static <T> CommonResponse<T> errorResponse(String errMsg){
        return new CommonResponse<>(ResponseCode.ERROR.getCode(), errMsg);
    }
    public static <T> CommonResponse<T> errorResponse(int errCode, String errMsg){
        return new CommonResponse<>(errCode, errMsg);
    }
}
