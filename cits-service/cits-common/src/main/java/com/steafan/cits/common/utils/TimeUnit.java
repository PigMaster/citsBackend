package com.steafan.cits.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author steafan
 * @Title: TimeUnit
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/2910:02
 */
public class TimeUnit {

    /**
     * 格式化当前时间
     *
     * @return
     */
    public static String currentTimeFormat(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date());
    }
}
