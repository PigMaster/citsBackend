package com.steafan.cits.common.config.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author steafan
 * @Title: RedisConfig
 * @ProjectName cits.pro
 * @Description: redis-pool配置
 * @date 2020/4/2610:21
 */
public class RedisConfig {

    private static JedisPool pool;
    private static Integer maxTotal = 20;
    private static Integer maxIdle = 20;
    private static Integer minIdle = 20;
    private static Boolean testOnBorrow = true;
    // 加了归还jedis连接是否可用的判断，就不用加这个参数了
    private static Boolean testOnReturn = false;


    private static final String HOST = "127.0.0.1";
    private static final int PORT = 6379;
    private static volatile Jedis jedis;

    private static void initPool(){
        JedisPoolConfig config = new JedisPoolConfig();
//        config.setMaxTotal(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMinIdle(minIdle);
        config.setTestOnBorrow(testOnBorrow);
        config.setTestOnReturn(testOnReturn);
        // 当个连接数超过redisPool时，再有的连接数将会阻塞，等待有新的连接时才会不阻塞,false会报异常
//        config.setBlockWhenExhausted(true);

        pool = new JedisPool(config, HOST, PORT, 1000*2);
    }

    static {
        initPool();
    }

    // 将不可用的jedis连接放到不可用的连接池中
    public static void returnBrokenResource(Jedis jedis){
        if (jedis != null){
            pool.returnBrokenResource(jedis);
        }
    }

    // 将可用的jedis连接放到可用的连接池中
    public static void returnResource(Jedis jedis){
        if (jedis != null){
            pool.returnResource(jedis);
        }
    }


    public static Jedis getJedis(){
        if (jedis == null){
            synchronized (RedisConfig.class){
                if (jedis == null){
                    jedis = new Jedis(HOST, PORT);
                }
                return jedis;
            }
        }
        return jedis;
    }


}
