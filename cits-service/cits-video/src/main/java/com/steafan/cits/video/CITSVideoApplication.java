package com.steafan.cits.video;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author steafan
 * @Title: VideoApplication
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/908:31
 */
@SpringBootApplication
@EnableDiscoveryClient
public class CITSVideoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CITSVideoApplication.class, args);
    }
}
