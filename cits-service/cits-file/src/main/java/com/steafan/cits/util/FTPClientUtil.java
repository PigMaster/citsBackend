package com.steafan.cits.util;

import sun.net.ftp.FtpClient;

/**
 * @author steafan
 * @Title: FTPClientUtil
 * @ProjectName cits.pro
 * @Description: double-check 基于线程安全的单例模式封装的vsftpd文件上传客户端对象
 * @date 2020/4/1310:41
 */
public class FTPClientUtil {
    private static volatile FTPClientUtil ftpClientUtil;

    private String serverHost;
    private String serverPort;
    private String ftpUser;
    private String ftpPwd;




    private FTPClientUtil(String serverHost, String serverPort, String ftpUser, String ftpPwd){
        this.serverHost = serverHost;
        this.serverPort = serverPort;
        this.ftpUser = ftpUser;
        this.ftpPwd = ftpPwd;
    }

    private FTPClientUtil(String serverHost, String ftpUser, String ftpPwd){
        this.serverHost = serverHost;
        this.ftpUser = ftpUser;
        this.ftpPwd = ftpPwd;
    }

    /**
     * 无端口连接
     *
     * @param serverHost
     * @param ftpUser
     * @param ftpPwd
     * @return
     */
    public static FTPClientUtil getFTPClient(String serverHost, String ftpUser, String ftpPwd){
        if (ftpClientUtil == null){
            synchronized (FTPClientUtil.class){
                if (ftpClientUtil == null){
                    ftpClientUtil = new FTPClientUtil(serverHost, ftpUser, ftpPwd);
                }
            }
        }
        return ftpClientUtil;
    }

    /**
     * 有端口连接
     *
     * @param serverHost
     * @param serverPort
     * @param ftpUser
     * @param ftpPwd
     * @return
     */
    public static FTPClientUtil getFTPClient(String serverHost, String serverPort, String ftpUser, String ftpPwd){
        if (ftpClientUtil == null){
            synchronized (FTPClientUtil.class){
                if (ftpClientUtil == null){
                    ftpClientUtil = new FTPClientUtil(serverHost, serverPort, ftpUser, ftpPwd);
                }
            }
        }
        return ftpClientUtil;
    }
}
