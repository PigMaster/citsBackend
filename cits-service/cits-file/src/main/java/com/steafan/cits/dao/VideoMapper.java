package com.steafan.cits.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.steafan.cits.pojo.Video;

/**
 * @author steafan
 * @Title: UserMapper
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/909:40
 */
public interface VideoMapper extends BaseMapper<Video> {


}
