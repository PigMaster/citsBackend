package com.steafan.cits.controller;

import com.github.pagehelper.PageInfo;
import com.netflix.discovery.converters.Auto;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.pojo.UserVideoComment;
import com.steafan.cits.service.IUserFileService;
import com.steafan.cits.vo.UserCommentedVideoVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author steafan
 * @Title: controller
 * @ProjectName cits.pro
 * @Description:
 * @date 2020/4/1211:01
 */
@Controller
@RequestMapping(value = "/userFile/", method = RequestMethod.POST)
public class UserFileController {

    @Autowired
    private IUserFileService userFileService;

    /**
     * 用户头像上传
     *
     * @param userIcon
     * @return
     * @throws IOException
     */
    @RequestMapping("userIconUpload.do")
    @ResponseBody
    @HystrixCommand(fallbackMethod = "userIconUploadFailed")
    public CommonResponse<String> userIconUpload(@Param("userIcon") MultipartFile userIcon) throws IOException{
        return userFileService.userIconUpload(userIcon);
    }

    /**
     * 发表视频评论，含星级评分，如果用户没有评论星级，则该视频默认5分，5分为满分
     *
     * @param userLoginToken
     * @param userVideoComment
     * @return
     */
    @RequestMapping(value = "user_comment_for_video.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<String> userCommentForVideo(String userLoginToken, @RequestBody UserVideoComment userVideoComment){
        return userFileService.userCommentForVideo(userLoginToken, userVideoComment);
    }

    /**
     * 获取用户已经评论过的视频数据和视频评论数据
     *
     * @param userLoginToken
     * @param username
     * @return
     */
    @RequestMapping(value = "get_user_video_commented.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<PageInfo> getUserVideoCommented(String userLoginToken, String username){
        return userFileService.getUserVideoCommented(userLoginToken, username);
    }
}
