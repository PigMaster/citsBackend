package com.steafan.cits.controller;

import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.pojo.UserVideoComment;
import com.steafan.cits.pojo.Video;
import com.steafan.cits.service.IFileService;
import com.steafan.cits.vo.VideoBaseAndCommentDataVO;
import com.steafan.cits.vo.VideoVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author steafan
 * @Title: FileController
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/2615:24
 */
@Controller
@RequestMapping("/file/")
public class FileController {

    @Autowired
    private IFileService fileService;

    /**
     * video文件上传,含入库
     *
     * @param videoFile
     * @param userLoginToken
     * @param videoVO
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "video_file_upload.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<Video> videoFileUpload(@Param("videoFile") MultipartFile videoFile, String userLoginToken, @RequestBody VideoVO videoVO) throws IOException{
        return fileService.videoFileUpload(videoFile, userLoginToken, videoVO);
    }

    /**
     * 获取当前视频的所有评论数据和详情数据
     *
     * @param videoId
     * @return
     */
    @RequestMapping(value = "get_video_base_comment.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<VideoBaseAndCommentDataVO> getVideoBaseAndComment(int videoId, String dataType){
        return fileService.getVideoBaseAndComment(videoId, dataType);
    }

    /**
     * 获取系统中所有的视频信息
     *
     * @return
     */
    @RequestMapping(value = "get_all_video_list.do", method = RequestMethod.GET)
    @ResponseBody
    public CommonResponse<List<Video>> getAllVideoList(){
        return fileService.getAllVideoList();
    }

    /**
     * pdf学习文件上传
     *
     * @param pdfFile
     * @param session
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "pdf_file_upload.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<String> pdfFileUpload(@Param("pdfFile") MultipartFile pdfFile, HttpSession session) throws IOException{
        return fileService.pdfFileUpload(pdfFile, session);
    }

    /**
     * excel学习文件上传
     *
     * @param excelFile
     * @param session
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "excel_file_upload.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<String> excelFileUpload(@Param("excelFile") MultipartFile excelFile, HttpSession session) throws IOException{
        return fileService.excelFileUpload(excelFile, session);
    }

    /**
     * word学习文件上传
     *
     * @param wordFile
     * @param session
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "word_file_upload.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<String> wordFileUpload(@Param("wordFile") MultipartFile wordFile, HttpSession session) throws IOException{
        return fileService.wordFileUpload(wordFile, session);
    }

    /**
     * ppt学习文件上传
     *
     * @param pptFile
     * @param session
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "ppt_file_upload.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<String> pptFileUpload(@Param("pptFile") MultipartFile pptFile, HttpSession session) throws IOException{
        return fileService.pptFileUpload(pptFile, session);
    }
}
