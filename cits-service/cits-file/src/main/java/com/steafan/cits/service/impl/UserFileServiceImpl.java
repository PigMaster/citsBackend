package com.steafan.cits.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.dao.UserVideoCommentMapper;
import com.steafan.cits.pojo.UserVideoComment;
import com.steafan.cits.pojo.Video;
import com.steafan.cits.service.IUserFileService;
import com.steafan.cits.service.user.utils.TokenUtil;
import com.steafan.cits.util.FTPClientUtil;
import com.steafan.cits.vo.UserCommentedVideoVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author steafan
 * @Title: UserFileServiceImpl
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/1211:02
 */
@Slf4j
@Service("userFileService")
public class UserFileServiceImpl implements IUserFileService {

    @Value("${cits-file.upload.host}")
    private String serverHost;
    @Value("${cits-file.upload.port}")
    private String serverPort;
    @Value("${cits-file.upload.user}")
    private String serverUser;
    @Value("${cits-file.upload.pwd}")
    private String serverPwd;

    private FTPClient ftpClient = new FTPClient();

    @Autowired
    private UserVideoCommentMapper userVideoCommentMapper;

    @Override
    public CommonResponse<String> userIconUpload(@Param("userIcon") MultipartFile userIcon) throws IOException {
        if (userIcon.isEmpty()){
            return CommonResponse.errorResponse("用户还没有选择任何头像");
        }
        // 文件名称拼接
        String fileTotalName = userIcon.getOriginalFilename();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
        String currDate = dateFormat.format(new Date());
        String uploadedFileName = "CITS-USER_ICON" + currDate + fileTotalName;
        // 连接FTP服务器并登录FTP服务器
        ftpClient.connect(serverHost);
        ftpClient.login(serverUser, serverPwd);
        ftpClient.changeWorkingDirectory("/ftpfile/");
        ftpClient.setBufferSize(1024);
        ftpClient.setControlEncoding("UTF-8");
        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();
        // 上传文件
        InputStream fileInputStream = userIcon.getInputStream();
        ftpClient.storeFile(uploadedFileName, fileInputStream);
        File fileDir = new File("/ftpfile/");
        if (!fileDir.exists()){
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }
        File targetFile = new File("/ftpfile/", uploadedFileName);
        ftpClient.disconnect();
        String userIconAddress = "http://" + serverHost + ":" + serverPort + "/" + targetFile.getName();
        if (StringUtils.isNotEmpty(userIconAddress)) {
            return CommonResponse.successResponse(userIconAddress);
        }
        return CommonResponse.errorResponse("文件服务器错误，上传用户头像失败，请联系系统管理员");
    }

    public CommonResponse<String> userCommentForVideo(String userLoginToken, @RequestBody UserVideoComment userVideoComment){
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录的token没有传递");
        }
        if (!TokenUtil.verify(userLoginToken)){
            return CommonResponse.errorResponse("用户没有登录，请登录后继续完成操作");
        }
        // 如果用户没有评论星级，则该视频默认5分，5分为满分
        if (userVideoComment.getVideoRate() == 0) {
            userVideoComment.setVideoRate(5);
        }
        userVideoComment.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        userVideoComment.setUpdateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        // 发表视频评论
        int resultCount = userVideoCommentMapper.insert(userVideoComment);
        if (resultCount > 0) {
            return CommonResponse.successResponse("发表视频评论成功，感谢您的观看");
        }
        return CommonResponse.errorResponse("系统错误，发表视频评论失败");
    }

    public CommonResponse<PageInfo> getUserVideoCommented(String userLoginToken, String username){
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录的token没有传递");
        }
        if (!TokenUtil.verify(userLoginToken)){
            return CommonResponse.errorResponse("用户没有登录，请登录后继续完成操作");
        }
        PageHelper.startPage(1, 5);
        List<UserCommentedVideoVO> videoCommentList = userVideoCommentMapper.selectVideoCommentByUsername(username);
        if (videoCommentList.size() > 0) {
            for (UserCommentedVideoVO userCommentedVideoVO : videoCommentList){
                String creteTime = userCommentedVideoVO.getCreateTime();
                String formateCreateTime = creteTime.substring(0,19);
                userCommentedVideoVO.setCreateTime(formateCreateTime);
            }
            PageInfo pageInfo = new PageInfo();
            pageInfo.setList(videoCommentList);
            return CommonResponse.successResponse(pageInfo);
//            return CommonResponse.successResponse(videoCommentList, videoCommentList.size());
        }
        return CommonResponse.errorResponse("您还没有评论过任何视频");
    }
}
