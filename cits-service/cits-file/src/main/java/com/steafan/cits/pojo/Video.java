package com.steafan.cits.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@TableName("cits_video")
public class Video implements Serializable {
    @TableId
    private int id;
    private String videoName;
    private String videoAddress;
    private String videoAuthor;
    private String videoIntro;
    private String videoType;
    private String videoSurfaceImg;
    private String createTime;
    private String updateTime;
}
