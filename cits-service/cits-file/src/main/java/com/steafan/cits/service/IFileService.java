package com.steafan.cits.service;

import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.pojo.UserVideoComment;
import com.steafan.cits.pojo.Video;
import com.steafan.cits.vo.VideoBaseAndCommentDataVO;
import com.steafan.cits.vo.VideoVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author steafan
 * @Title: IFileService
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/2615:23
 */
public interface IFileService {

    CommonResponse<Video> videoFileUpload(@Param("videoFile") MultipartFile videoFile, String userLoginToken, @RequestBody VideoVO videoVO) throws IOException;

    CommonResponse<VideoBaseAndCommentDataVO> getVideoBaseAndComment(int videoId, String dataType);

    CommonResponse<String> pdfFileUpload(@Param("pdfFile") MultipartFile pdfFile, HttpSession session) throws IOException;

    CommonResponse<String> excelFileUpload(@Param("excelFile") MultipartFile excelFile, HttpSession session) throws IOException;

    CommonResponse<String> wordFileUpload(@Param("wordFile") MultipartFile wordFile, HttpSession session) throws IOException;

    CommonResponse<String> pptFileUpload(@Param("pptFile") MultipartFile pptFile, HttpSession session) throws IOException;

    CommonResponse<List<Video>> getAllVideoList();
}
