package com.steafan.cits.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.steafan.cits.common.commons.CommonConst;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.common.utils.TimeUnit;
import com.steafan.cits.dao.UserVideoCommentMapper;
import com.steafan.cits.dao.VideoMapper;
import com.steafan.cits.pojo.UserVideoComment;
import com.steafan.cits.pojo.Video;
import com.steafan.cits.service.IFileService;
import com.steafan.cits.service.user.utils.TokenUtil;
import com.steafan.cits.vo.VideoBaseAndCommentDataVO;
import com.steafan.cits.vo.VideoVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author steafan
 * @Title: FileServiceImpl
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/2615:23
 */
@Service("fileService")
public class FileServiceImpl implements IFileService {

    @Value("${cits-file.upload.host}")
    private String serverHost;
    @Value("${cits-file.upload.port}")
    private String serverPort;
    @Value("${cits-file.upload.user}")
    private String serverUser;
    @Value("${cits-file.upload.pwd}")
    private String serverPwd;

    private FTPClient ftpClient = new FTPClient();

    @Autowired
    private VideoMapper videoMapper;
    @Autowired
    private UserVideoCommentMapper userVideoCommentMapper;

    public CommonResponse<VideoBaseAndCommentDataVO> getVideoBaseAndComment(int videoId, String dataType) {
        VideoBaseAndCommentDataVO videoBaseAndCommentDataVO = new VideoBaseAndCommentDataVO();
        QueryWrapper queryWrapper = new QueryWrapper();
        switch (dataType) {
            case "base":
                queryWrapper.eq("id", videoId);
                // 获取视频详情
                Video videoDetail = videoMapper.selectOne(queryWrapper);
                videoBaseAndCommentDataVO.setVideo(videoDetail);
                return CommonResponse.successResponse(videoBaseAndCommentDataVO);
            case "topic":
                queryWrapper.eq("video_id", videoId);
                // 获取视频评论
                List<UserVideoComment> videoCommentList = userVideoCommentMapper.selectList(queryWrapper);
                if (videoCommentList.size() > 0) {
                    for (UserVideoComment userVideoComment : videoCommentList) {
                        String createTime = userVideoComment.getCreateTime();
                        String formateTime = createTime.substring(0,19);
                        userVideoComment.setCreateTime(formateTime);
                    }
                    videoBaseAndCommentDataVO.setVideoCommentList(videoCommentList);
                    return CommonResponse.successResponse(videoBaseAndCommentDataVO);
                }
                return CommonResponse.errorResponse("该视频还没有任何评论");
            default:
                return null;
        }
    }

    @Override
    public CommonResponse<Video> videoFileUpload(@Param("videoFile") MultipartFile videoFile, String userLoginToken, @RequestBody VideoVO videoVO) throws IOException {
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录的token没有传递");
        }
        if (!TokenUtil.verify(userLoginToken)) {
            return CommonResponse.errorResponse("用户没有登录，请登录后继续完成操作");
        }
        // 文件名称拼接
        String fileTotalName = videoFile.getOriginalFilename();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
        String currDate = dateFormat.format(new Date());
        String uploadedFileName = "CITS-STUDYING_VIDEO" + currDate + fileTotalName;
        ftpClient.connect(serverHost);
        ftpClient.login(serverUser, serverPwd);
        ftpClient.changeWorkingDirectory("/ftpfile/");
        ftpClient.setBufferSize(1024 * 20);
        ftpClient.setControlEncoding("UTF-8");
        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();
        InputStream pdfStream = videoFile.getInputStream();
        ftpClient.storeFile(uploadedFileName, pdfStream);
        File fileDir = new File("/ftpfile/");
        if (!fileDir.exists()) {
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }
        File targetFile = new File("/ftpfile/", uploadedFileName);
        ftpClient.disconnect();
        String videoAddress = "http://" + serverHost + ":" + serverPort + "/" + targetFile.getName();
        // 入库
        Video uploadVideo = new Video();
        uploadVideo.setVideoName(videoVO.getVideoName());
        uploadVideo.setVideoAddress(videoAddress);
        uploadVideo.setVideoAuthor(videoVO.getVideoAuthor());
        uploadVideo.setVideoIntro(videoVO.getVideoIntro());
        uploadVideo.setVideoType(videoVO.getVideoType());
        uploadVideo.setVideoSurfaceImg(videoVO.getVideoSurfaceImage());

        int resultCount = videoMapper.insert(uploadVideo);
        if (resultCount > 0) {
            return CommonResponse.successResponse(uploadVideo);
        }

        return CommonResponse.errorResponse("系统错误，上传视频失败");
    }

    public CommonResponse<List<Video>> getAllVideoList() {
        List<Video> videoList = videoMapper.selectList(null);
        if (videoList.size() > 0) {
            return CommonResponse.successResponse(videoList);
        }
        return CommonResponse.errorResponse("系统中还没有任何视频，赶快来上传吧");
    }

    @Override
    public CommonResponse<String> pdfFileUpload(@Param("pdfFile") MultipartFile pdfFile, HttpSession session) throws IOException {
        // 文件名称拼接
        String fileTotalName = pdfFile.getOriginalFilename();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
        String currDate = dateFormat.format(new Date());
        String uploadedFileName = "CITS-STUDYING_PDF" + currDate + fileTotalName;

        ftpClient.connect(serverHost);
        ftpClient.login(serverUser, serverPwd);
        ftpClient.changeWorkingDirectory("/ftpfile/");
        ftpClient.setBufferSize(1024 * 20);
        ftpClient.setControlEncoding("UTF-8");
        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();

        InputStream pdfStream = pdfFile.getInputStream();
        ftpClient.storeFile(uploadedFileName, pdfStream);

        File fileDir = new File("/ftpfile/");
        if (!fileDir.exists()) {
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }

        File targetFile = new File("/ftpfile/", uploadedFileName);


        ftpClient.disconnect();

        String pdfName = "http://" + serverHost + ":" + serverPort + "/" + targetFile.getName();

        return CommonResponse.successResponse(pdfName);

    }

    @Override
    public CommonResponse<String> excelFileUpload(@Param("excelFile") MultipartFile excelFile, HttpSession session) throws IOException {
        // 文件名称拼接
        String fileTotalName = excelFile.getOriginalFilename();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
        String currDate = dateFormat.format(new Date());
        String uploadedFileName = "CITS-STUDYING_EXCEL" + currDate + fileTotalName;

        ftpClient.connect(serverHost);
        ftpClient.login(serverUser, serverPwd);
        ftpClient.changeWorkingDirectory("/ftpfile/");
        ftpClient.setBufferSize(1024 * 20);
        ftpClient.setControlEncoding("UTF-8");
        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();

        InputStream pdfStream = excelFile.getInputStream();
        ftpClient.storeFile(uploadedFileName, pdfStream);

        File fileDir = new File("/ftpfile/");
        if (!fileDir.exists()) {
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }

        File targetFile = new File("/ftpfile/", uploadedFileName);


        ftpClient.disconnect();

        String pdfName = "http://" + serverHost + ":" + serverPort + "/" + targetFile.getName();
        return CommonResponse.errorResponse("系统错误，上传excel学习文件失败，请联系系统管理员");

    }

    @Override
    public CommonResponse<String> wordFileUpload(@Param("wordFile") MultipartFile wordFile, HttpSession session) throws IOException {

        // 文件名称拼接
        String fileTotalName = wordFile.getOriginalFilename();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
        String currDate = dateFormat.format(new Date());
        String uploadedFileName = "CITS-STUDYING_WORD" + currDate + fileTotalName;

        ftpClient.connect(serverHost);
        ftpClient.login(serverUser, serverPwd);
        ftpClient.changeWorkingDirectory("/ftpfile/");
        ftpClient.setBufferSize(1024 * 20);
        ftpClient.setControlEncoding("UTF-8");
        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();

        InputStream pdfStream = wordFile.getInputStream();
        ftpClient.storeFile(uploadedFileName, pdfStream);

        File fileDir = new File("/ftpfile/");
        if (!fileDir.exists()) {
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }

        File targetFile = new File("/ftpfile/", uploadedFileName);


        ftpClient.disconnect();

        String pdfName = "http://" + serverHost + ":" + serverPort + "/" + targetFile.getName();
        return CommonResponse.errorResponse("系统错误，上传excel学习文件失败，请联系系统管理员");

    }

    @Override
    public CommonResponse<String> pptFileUpload(@Param("pptFile") MultipartFile pptFile, HttpSession session) throws IOException {
        // 文件名称拼接
        String fileTotalName = pptFile.getOriginalFilename();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
        String currDate = dateFormat.format(new Date());
        String uploadedFileName = "CITS-STUDYING_PPT" + currDate + fileTotalName;

        ftpClient.connect(serverHost);
        ftpClient.login(serverUser, serverPwd);
        ftpClient.changeWorkingDirectory("/ftpfile/");
        ftpClient.setBufferSize(1024 * 20);
        ftpClient.setControlEncoding("UTF-8");
        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();

        InputStream pdfStream = pptFile.getInputStream();
        ftpClient.storeFile(uploadedFileName, pdfStream);

        File fileDir = new File("/ftpfile/");
        if (!fileDir.exists()) {
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }

        File targetFile = new File("/ftpfile/", uploadedFileName);


        ftpClient.disconnect();

        String pdfName = "http://" + serverHost + ":" + serverPort + "/" + targetFile.getName();
        return CommonResponse.errorResponse("系统错误，上传excel学习文件失败，请联系系统管理员");

    }

}
