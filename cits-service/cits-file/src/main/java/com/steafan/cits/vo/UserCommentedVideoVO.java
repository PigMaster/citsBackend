package com.steafan.cits.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserCommentedVideoVO {
    private String videoName;
    private String videoAddress;
    private int videoRate;
    private String videoBack;
    private String createTime;
}
