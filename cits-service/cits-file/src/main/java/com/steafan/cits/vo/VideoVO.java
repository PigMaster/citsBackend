package com.steafan.cits.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VideoVO {
    private String username;
    private String videoName;
    private String videoAuthor;
    private String videoIntro;
    private String videoType;
    private String videoSurfaceImage;
}
