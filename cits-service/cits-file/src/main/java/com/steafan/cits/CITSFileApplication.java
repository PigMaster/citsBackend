package com.steafan.cits;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * @author steafan
 * @Title: FileApplication
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/908:18
 */
@SpringBootApplication
@EnableHystrix
@MapperScan("com.steafan.cits.dao")
@EnableDiscoveryClient
public class CITSFileApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(CITSFileApplication.class).run(args);
    }
}
