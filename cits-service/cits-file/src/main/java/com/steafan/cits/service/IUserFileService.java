package com.steafan.cits.service;

import com.github.pagehelper.PageInfo;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.pojo.UserVideoComment;
import com.steafan.cits.vo.UserCommentedVideoVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author steafan
 * @Title: IUserFileService
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/1211:02
 */
public interface IUserFileService {

    CommonResponse<String> userIconUpload(@Param("userIcon") MultipartFile userIcon) throws IOException;

    CommonResponse<String> userCommentForVideo(String userLoginToken, @RequestBody UserVideoComment userVideoComment);

    CommonResponse<PageInfo> getUserVideoCommented(String userLoginToken, String username);
}
