package com.steafan.cits.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.steafan.cits.pojo.UserVideoComment;
import com.steafan.cits.vo.UserCommentedVideoVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserVideoCommentMapper extends BaseMapper<UserVideoComment> {

    List<UserCommentedVideoVO> selectVideoCommentByUsername(String username);
}
