package com.steafan.cits.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@TableName("cits_video_comment")
public class UserVideoComment implements Serializable {
    @TableId
    private int id;
    private int videoId;
    private int videoRate;
    private String videoBack;
    private String videoBackUser;
    private String videoBackUserIcon;
    private String createTime;
    private String updateTime;
}
