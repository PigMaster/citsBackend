package com.steafan.cits.service.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.steafan.cits.service.user.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: UserMapper
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/909:40
 */
public interface UserMapper extends BaseMapper<User> {

    int selectUserByUsername(String username);

    User selectUserLoginByUsernamePassword(@Param("userLogin") User userLogin);

    int selectEmailAndPhoneByUserInfo(@Param("userRegister") User userRegister);

    User selectUserDetailInfoByUserId(int userId);

    int selectUserQuestionByUsername(@Param("username") String username, @Param("question") String question);

    int selectUserQuestionAnswerByUsername(@Param("username") String username, @Param("question") String question, @Param("answer") String answer);

    int updateUserForgetPwdByUsername(@Param("username") String username, @Param("passwordNew") String passwordNew);

}
