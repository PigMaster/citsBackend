package com.steafan.cits.service.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.service.user.pojo.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author steafan
 * @Title: IUserService
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/717:01
 */
//@FeignClient(value = "cits-eureka-client-user-service", fallback = UserServiceHystrix.class)
public interface IUserService extends IService<User> {

    CommonResponse<User> login(HttpSession session, @RequestBody User user);

    CommonResponse<String> logout(HttpSession session);

    CommonResponse<String> register(@RequestBody User user);

    CommonResponse<User> getUserInfo(int userId);

    CommonResponse<String> checkQuestion(String username, String question);

    CommonResponse<String> checkAnswerAndResetForgetPwd(String username, String question, String answer, String forgetToken, String passwordNew);

    CommonResponse<String> resetPassword(String userLoginToken, @RequestBody User resetPasswordUser, String passwordNew);

//    CommonResponse<String> modifiedUserInfo(HttpSession session, User modifiedInfoUser);
    CommonResponse<String> modifiedUserInfo(String userLoginToken, @RequestBody User modifiedInfoUser);

    CommonResponse<String> userLiking(HttpSession session, @RequestBody UserLike userLike);

    CommonResponse<String> userCommenting(HttpSession session, @RequestBody UserComment userComment);

    CommonResponse<String> userCommentReply(HttpSession session, @RequestBody UserCommentReply userCommentReply);

    CommonResponse<List<UserLike>> getAllUserLike(HttpSession session);

    CommonResponse<List<UserComment>> getAllUserComment(HttpSession session);

    CommonResponse<String> userFavorite(HttpSession session, @RequestBody UserFavorite userFavorite);

    CommonResponse<List<UserFavorite>> getUserFavoriteList(HttpSession session);

    CommonResponse<List<UserVideo>> getUserVideoList(HttpSession session);

    CommonResponse<List<UserQuestionBank>> getUserQuestionBankList(HttpSession session);
}
