package com.steafan.cits.service.user.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: UserLike
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/2909:43
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@TableName("cits_user_like")
public class UserLike implements Serializable {
    @TableId
    private int id;
    private String username;
    private int likeId;
    private int likeType;
    private int likeStatus;
    private String createTime;
    private String updateTime;
}
