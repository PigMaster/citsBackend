package com.steafan.cits.service.user.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import lombok.*;
import org.apache.ibatis.reflection.wrapper.BaseWrapper;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: User
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/909:40
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@TableName("cits_user")
public class User implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private int id;
    private String username;
    private String password;
    private String icon;
    private String sex;
    private String phone;
    private String email;
    private String question;
    private String answer;
    private int role;
    private String createTime;
    private String updateTime;
}
