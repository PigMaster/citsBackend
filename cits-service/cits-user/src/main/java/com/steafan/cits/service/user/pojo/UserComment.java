package com.steafan.cits.service.user.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: UserComment
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/708:42
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@TableName("cits_user_comment")
public class UserComment implements Serializable {
    @TableId
    private int id;
    private String username;
    private int typeId;
    private int typeDetailId;
    private String commentContent;
    private String commentTime;
    private String updateTime;
}
