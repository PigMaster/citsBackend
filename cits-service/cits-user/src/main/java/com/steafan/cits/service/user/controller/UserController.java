package com.steafan.cits.service.user.controller;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.naming.NamingService;
import com.netflix.discovery.DiscoveryClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.service.user.pojo.*;
import com.steafan.cits.service.user.service.IUserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author steafan
 * @Title: UserController
 * @ProjectName cits.pro
 * @Description:
 * @date 2020/4/717:01
 */
@Controller
@RequestMapping(value = "/user/", method = RequestMethod.POST)
//@Api(value = "user-controller", tags = "user", description = "所有用户业务接口必须使用post请求", hidden = true)
@Api(hidden = true)
public class UserController {

    @Autowired
    private IUserService userService;


    /**
     * 用户登录
     *
     * @param session
     * @param user
     * @return
     */
    @RequestMapping("login.do")
    @ResponseBody
    @ApiOperation(value = "用户登录")
    @ApiParam(name = "user", required = true)
    public CommonResponse<User> login(HttpSession session, @RequestBody User user){
        return userService.login(session, user);
    }


    /**
     * 用户退出登录
     *
     * @param session
     * @return
     */
    @RequestMapping("logout.do")
    @ResponseBody
    @ApiOperation(value = "用户退出登录")
    public CommonResponse<String> logout(HttpSession session){
        return userService.logout(session);
    }

    /**
     * 用户注册-带头像上传
     *
     * @param user
     * @return
     */
    @RequestMapping("register.do")
    @ResponseBody
    @HystrixCommand(fallbackMethod = "register_failed")
    @ApiOperation(value = "用户注册")
    @ApiParam(name = "user", required = true)
    public CommonResponse<String> register(@RequestBody User user){
        return userService.register(user);
    }

    /**
     * 获取具体用户详细信息
     *
     * @param userId
     * @return
     */
    @RequestMapping("get_user_info.do")
    @ResponseBody
    @ApiOperation(value = "获取具体用户详细信息")
    @ApiParam(name = "userId", required = true)
    public CommonResponse<User> getUserInfo(int userId) {
        return userService.getUserInfo(userId);
    }

    /**
     * 忘记密码的找回密码-检验用户问题
     *
     * @param username
     * @param question
     * @return
     */
    @RequestMapping("check_question.do")
    @ResponseBody
    @ApiOperation(value = "忘记密码的找回密码-检验用户找回密码的问题")
    public CommonResponse<String> checkQuestion(
            @ApiParam(name = "username", required = true) String username,
            @ApiParam(name = "question", required = true) String question){
        return userService.checkQuestion(username, question);
    }

    /**
     * 忘记密码的找回密码-检验用户问题答案并找回密码
     *
     * @param username
     * @param forgetToken
     * @param passwordNew
     * @return
     */
    @RequestMapping("check_answer_and_reset_pwd.do")
    @ResponseBody
    @ApiOperation(value = "忘记密码的找回密码-检验用户问题答案并找回密码")
    public CommonResponse<String> checkAnswerAndResetForgetPwd(
            String username, String question, String answer, String forgetToken, String passwordNew){
        return userService.checkAnswerAndResetForgetPwd(username, question, answer, forgetToken, passwordNew);
    }

    /**
     * 记住密码的修改密码
     *
     * @param userLoginToken
     * @param resetPasswordUser
     * @param passwordNew
     * @return
     */
    @RequestMapping("reset_pwd.do")
    @ResponseBody
    @ApiOperation(value = "记住密码的修改密码-个人中心直接修改")
    public CommonResponse<String> resetPassword(
            String userLoginToken, @RequestBody User resetPasswordUser, String passwordNew){
        return userService.resetPassword(userLoginToken, resetPasswordUser, passwordNew);
    }

    /**
     * 修改用户个人信息
     * 用户修改头像，只要重新调用头像上传接口并更新url即可
     *
     * @param userLoginToken
     * @param modifiedInfoUser
     * @return
     */
    @RequestMapping("modified_user_info.do")
    @ResponseBody
    @ApiOperation(value = "修改个人信息")
    public CommonResponse<String> modifiedUserInfo(String userLoginToken, @RequestBody User modifiedInfoUser){
        return userService.modifiedUserInfo(userLoginToken, modifiedInfoUser);
    }

    /**
     * 用户点赞
     *
     * @param session
     * @param userLike
     * @return
     */
    @RequestMapping("user_liking.do")
    @ResponseBody
    public CommonResponse<String> userLiking(
            @ApiParam(value = "session", required = true) HttpSession session,
            @ApiParam(value = "userLike", required = true) @RequestBody UserLike userLike){
        return userService.userLiking(session, userLike);
    }

    /**
     * 用户发表评论
     *
     * @param session
     * @param userComment
     * @return
     */
    @RequestMapping("user_commenting.do")
    @ResponseBody
    @ApiOperation(value = "用户发表评论")
    public CommonResponse<String> userCommenting(
            @ApiParam(value = "session", required = true) HttpSession session,
            @ApiParam(value = "userComment", required = true) @RequestBody UserComment userComment){
        return userService.userCommenting(session, userComment);
    }

    /**
     * 用户回复评论
     *
     * @param session
     * @param userCommentReply
     * @return
     */
    @RequestMapping("user_comment_reply.do")
    @ResponseBody
    @ApiOperation(value = "用户回复评论")
    public CommonResponse<String> userCommentReply(
            @ApiParam(value = "session", required = true) HttpSession session,
            @ApiParam(value = "userCommentReply", required = true) @RequestBody UserCommentReply userCommentReply){
        return userService.userCommentReply(session, userCommentReply);
    }

    /**
     * 如果用户未登录-->获取所有点赞数据
     * 如果用户已登录-->获取当前登录用户的点赞数据
     *
     * @return
     */
    @RequestMapping("get_all_user_like.do")
    @ResponseBody
    public CommonResponse<List<UserLike>> getAllUserLike(HttpSession session){
        return userService.getAllUserLike(session);
    }

    /**
     * 如果用户未登录-->获取所有评论数据
     * 如果用户已登录-->获取当前登录用户的评论数据
     *
     * @param session
     * @return
     */
    @RequestMapping("get_all_user_comment.do")
    @ResponseBody
    public CommonResponse<List<UserComment>> getAllUserComment(HttpSession session){
        return userService.getAllUserComment(session);
    }

    /**
     * 获取用户观看视频
     *
     * @param session
     * @return
     */
    @RequestMapping("get_user_video.do")
    @ResponseBody
    public CommonResponse<List<UserVideo>> getUserVideoList(HttpSession session){
        return userService.getUserVideoList(session);
    }

    /**
     * 获取用户题目
     *
     * @param session
     * @return
     */
    @RequestMapping("get_user_question_bank.do")
    @ResponseBody
    public CommonResponse<List<UserQuestionBank>> getUserQuestionBankList(HttpSession session){
        return userService.getUserQuestionBankList(session);
    }

}
