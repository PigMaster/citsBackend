package com.steafan.cits.service.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.steafan.cits.service.user.pojo.User;
import com.steafan.cits.service.user.pojo.UserLike;
import org.apache.ibatis.annotations.Param;

/**
 * @author steafan
 * @Title: UserMapper
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/909:40
 */
public interface UserLikeMapper extends BaseMapper<UserLike> {


    int selectUserLikeDataByUsernameLikeIdAndLikeType(
            @Param("username") String username,
            @Param("likeId") int likeId,
            @Param("likeType") int likeType
            );

    int selectUserLikeStatusByLikeIdLikeTypeAndUsername(
            @Param("username") String username,
            @Param("likeId") int likeId,
            @Param("likeType") int likeType
    );


    int updateUserLikeStatusAndCountsByUsernameLikeIdAndLikeType(@Param("userLike") UserLike userLike);

}
