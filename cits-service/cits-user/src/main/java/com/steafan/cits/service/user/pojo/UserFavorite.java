package com.steafan.cits.service.user.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: UserFavorite
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/709:49
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@TableName("cits_user_favorite")
public class UserFavorite implements Serializable {
    @TableId
    private int id;
    private String username;
    private int favoriteTypeId;
    private int favoriteTypeDetailId;
    private String createTime;
    private String updateTime;
}
