package com.steafan.cits.service.user;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.naming.NamingService;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author steafan
 * @Title: CITSUserApplication
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/710:11
 */
@SpringBootApplication
//@EnableEurekaClient
@MapperScan("com.steafan.cits.service.user.dao")
//@EnableCircuitBreaker
//@EnableHystrix
//@EnableHystrixDashboard
//@EnableTurbine
//@EnableScheduling
@EnableDiscoveryClient
public class CITSUserApplication {

//    @Bean
//    public MapperScannerConfigurer mapperScannerConfigurer(){
//        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
//        mapperScannerConfigurer.setBasePackage("com.steafan.cits.service.user.dao");
//        return mapperScannerConfigurer;
//    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(CITSUserApplication.class).run(args);
    }
}
