package com.steafan.cits.service.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.steafan.cits.service.user.pojo.UserComment;
import com.steafan.cits.service.user.pojo.UserVideo;

/**
 *
 *
 */
public interface UserVideoMapper extends BaseMapper<UserVideo> {


}
