package com.steafan.cits.service.user.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: UserVideo
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/716:33
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("cits_user_video")
public class UserVideo implements Serializable {
    @TableId
    private int id;
    private String username;
    private int videoId;
    private String videoAddress;
    private String currentTime;
    private String createTime;
    private String udpateTime;
}
