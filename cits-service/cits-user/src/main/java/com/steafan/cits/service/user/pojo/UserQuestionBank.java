package com.steafan.cits.service.user.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: UserQuestionBank
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/1110:43
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@TableName("cits_user_question_book")
public class UserQuestionBank implements Serializable {
    @TableId
    private int id;
    private String username;
    private int questionId;
    private int questionType;
    private String result;
    private String createTime;
    private String updateTime;
}
