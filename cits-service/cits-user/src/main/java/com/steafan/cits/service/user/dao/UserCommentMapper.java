package com.steafan.cits.service.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.steafan.cits.service.user.pojo.UserComment;
import com.steafan.cits.service.user.pojo.UserLike;
import org.apache.ibatis.annotations.Param;

/**
 *
 *
 */
public interface UserCommentMapper extends BaseMapper<UserComment> {


}
