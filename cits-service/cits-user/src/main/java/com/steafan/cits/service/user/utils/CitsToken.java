package com.steafan.cits.service.user.utils;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @author steafan
 * @Title: CitsToken
 * @ProjectName cits.pro
 * @Description: token生成工具类
 * @date 2020/4/2014:13
 */
@Slf4j
public class CitsToken {

    private static LoadingCache<String, String> localCache = CacheBuilder.newBuilder().initialCapacity(1000).maximumSize(10000).expireAfterAccess(10, TimeUnit.MINUTES)
            .build(new CacheLoader<String, String>() {
                // 默认数据加载实现，当调用get取值时，如果key没有对应的值，就调用load方法处理
                @Override
                public String load(String key) throws Exception {
                    return "null";
                }
            });

    public static void setKey(String key, String value){
        localCache.put(key, value);
    }

    public static String getKey(String key){
        String value = null;
        try {
            value = localCache.get(key);
            if ("null".equals(value)){
                return null;
            }
            return value;
        } catch (Exception e){
            log.error("localcache get error", e);
        }
        return null;
    }

//    public static void main(String[] args) {
//        CitsToken.setKey("steafan", "demo to generate token");
//        log.info(CitsToken.getKey("steafan"));
//    }
}
