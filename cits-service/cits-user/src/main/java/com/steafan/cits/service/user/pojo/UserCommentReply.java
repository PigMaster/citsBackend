package com.steafan.cits.service.user.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * @author steafan
 * @Title: UserCommentReply
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/5/709:06
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("cits_user_comment_reply")
public class UserCommentReply implements Serializable {
    @TableId
    private int id;
    private String replyUser;
    private int commentId;
    private String commentUser;
    private String replyContent;
    private String replyTime;
    private String updateTime;
}
