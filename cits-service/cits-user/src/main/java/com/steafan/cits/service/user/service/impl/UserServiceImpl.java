package com.steafan.cits.service.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.netflix.discovery.converters.Auto;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
//import com.rabbitmq.client.DeliverCallback;
import com.steafan.cits.common.commons.CommonConst;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.common.commons.ResponseCode;
import com.steafan.cits.common.config.redis.RedisConfig;
import com.steafan.cits.common.config.redis.RedisKeyConfig;
import com.steafan.cits.common.utils.TimeUnit;
import com.steafan.cits.service.user.dao.*;
import com.steafan.cits.service.user.pojo.*;
import com.steafan.cits.service.user.service.IUserService;
import com.steafan.cits.service.user.utils.CitsToken;
import com.steafan.cits.service.user.utils.MD5Util;
import com.steafan.cits.service.user.utils.RedisUtil;
import com.steafan.cits.service.user.utils.TokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import sun.security.provider.MD5;

import javax.print.DocFlavor;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * @author steafan
 * @Title: UserServiceImpl
 * @ProjectName cits.pro
 * @Description: cits-用户服务实现
 * @date 2020/4/717:02
 */
@Slf4j
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserLikeMapper userLikeMapper;
    @Autowired
    private UserCommentMapper userCommentMapper;
    @Autowired
    private UserCommentReplyMapper userCommentReplyMapper;
    @Autowired
    private UserFavoriteMapper userFavoriteMapper;
    @Autowired
    private UserVideoMapper userVideoMapper;
    @Autowired
    private UserQuestionBankMapper userQuestionBankMapper;

//    @Value("${spring.rabbitmq.host}")
//    private String rabbitMqHost;

    private static final String LOGIN_USER_SESSION_QUEUE = "userLoginSession";
    Channel channel = null;

    @Override
    public CommonResponse<User> login(HttpSession session, @RequestBody User user) {
        int resultCount = userMapper.selectUserByUsername(user.getUsername());
        if (resultCount > 0) {
            user.setPassword(MD5Util.getMD5(MD5Util.getMD5(user.getPassword())));
            User loginUser = userMapper.selectUserLoginByUsernamePassword(user);
            if (null == loginUser) {
                return CommonResponse.errorResponse("登录密码错误，请重新输入");
            }
            session.setAttribute(CommonConst.CURRENT_USER, loginUser);
            // 生成token
            String userLoginToken = TokenUtil.token(loginUser.getUsername(), loginUser.getPassword());
            loginUser.setPassword("");
            return CommonResponse.successResponse(userLoginToken, loginUser);
        }
        return CommonResponse.errorResponse("用户不存在，请核实");
    }

    @Override
    public CommonResponse<String> logout(HttpSession session) {
        session.removeAttribute(CommonConst.CURRENT_USER);
        return CommonResponse.successResponse("退出登录成功");
    }

    @Override
    public CommonResponse<String> register(@RequestBody User user) {
        int resultCount = userMapper.selectEmailAndPhoneByUserInfo(user);
        if (resultCount > 0) {
            return CommonResponse.errorResponse("您输入的邮箱或手机已存在，请重新输入");
        }
        User registerUser = new User();
        registerUser.setUsername(user.getUsername());
        registerUser.setPassword(MD5Util.getMD5(MD5Util.getMD5(user.getPassword())));
        // 用户头像上传：由用户头像上传接口返回上传头像地址
        registerUser.setIcon(user.getIcon());
        registerUser.setEmail(user.getEmail());
        registerUser.setPhone(user.getPhone());
        registerUser.setQuestion(user.getQuestion());
        registerUser.setAnswer(user.getAnswer());
        registerUser.setSex(user.getSex());
        registerUser.setRole(CommonConst.USER_ROLE.STUDENT);

        int registerResultCount = userMapper.insert(registerUser);
        if (registerResultCount > 0) {
            return CommonResponse.successResponse("用户注册成功");
        }
        return CommonResponse.errorResponse("系统错误，用户注册失败");
    }

    @Override
    public CommonResponse<User> getUserInfo(int userId) {
        User userDetail = userMapper.selectUserDetailInfoByUserId(userId);
        if (userDetail == null) {
            return CommonResponse.errorResponse("系统中还没有该用户");
        }
        return CommonResponse.successResponse(userDetail);
    }


    @Override
    public CommonResponse<String> checkQuestion(String username, String question) {
        int resultCount = userMapper.selectUserQuestionByUsername(username, question);
        if (resultCount > 0) {
            // 检测成功，进入忘记密码的找回密码界面
            String tokenUUID = UUID.randomUUID().toString();
            CitsToken.setKey("cits_token_" + username, tokenUUID);
            return CommonResponse.successResponse(tokenUUID);
        }
        return CommonResponse.errorResponse("用户不存在或找回密码的问题为空");
    }

    @Override
    public CommonResponse<String> checkAnswerAndResetForgetPwd(String username, String question, String answer, String forgetToken, String passwordNew) {
        if (StringUtils.isEmpty(forgetToken)) {
            return CommonResponse.errorResponse("token没有传递，无法修改密码");
        }
        String token = CitsToken.getKey("cits_token_" + username);
        if (!StringUtils.equals(token, forgetToken)) {
            return CommonResponse.errorResponse("token传递错误，无法修改密码");
        }
        int checkAnswerCount = userMapper.selectUserQuestionAnswerByUsername(username, question, answer);
        if (checkAnswerCount > 0){
            int resultCount =
                    userMapper.updateUserForgetPwdByUsername(username, MD5Util.getMD5(MD5Util.getMD5(passwordNew)));
            if (resultCount > 0) {
                return CommonResponse.successResponse("密码找回成功");
            }
            return CommonResponse.errorResponse("系统错误，密码找回失败，请联系系统管理员");
        }
        return CommonResponse.errorResponse("找回密码问题的答案错误");

    }


    @Override
    public CommonResponse<String> resetPassword(String userLoginToken, @RequestBody User resetPasswordUser, String passwordNew) {
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录凭证未传递");
        }
        if (!TokenUtil.verify(userLoginToken)) {
            return CommonResponse.errorResponse("用户未登录，无法进行此操作");
        }
        int userResult = userMapper.selectUserByUsername(resetPasswordUser.getUsername());
        if (userResult > 0) {
            resetPasswordUser.setPassword(MD5Util.getMD5(MD5Util.getMD5(resetPasswordUser.getPassword())));
            User resultUser = userMapper.selectUserLoginByUsernamePassword(resetPasswordUser);
            if (resultUser == null) {
                return CommonResponse.errorResponse("旧密码错误");
            }
            int resultCount = userMapper.
                    updateUserForgetPwdByUsername(resetPasswordUser.getUsername(), MD5Util.getMD5(MD5Util.getMD5(passwordNew)));
            if (resultCount > 0) {
                return CommonResponse.successResponse("修改密码成功");
            }
            return CommonResponse.errorResponse("系统错误，修改密码失败，请联系系统管理员");
        }
        return CommonResponse.errorResponse("当前登录用户非法，无法修改密码");
    }

    @Override
    public CommonResponse<String> modifiedUserInfo(String userLoginToken, @RequestBody User modifiedInfoUser) {
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录的凭证没有传递");
        }
        boolean tokenVerifyResult = TokenUtil.verify(userLoginToken);
        if (!tokenVerifyResult) {
            return CommonResponse.errorResponse("用户登录状态已过期，请重新登录");
        }
        int resultCount = userMapper.updateById(modifiedInfoUser);
        if (resultCount > 0) {
            return CommonResponse.successResponse("修改个人信息成功");
        }
        return CommonResponse.errorResponse("系统错误，修改个人信息失败，请联系系统管理员");
    }

    @Override
    public CommonResponse<String> userLiking(HttpSession session, @RequestBody UserLike userLike) {
        User loginUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        if (null == loginUser) {
            return CommonResponse.errorResponse("您还没有登录，无法点赞哦");
        }
        UserLike like = new UserLike();
        like.setUsername(userLike.getUsername());
        like.setLikeId(userLike.getLikeId());
        like.setLikeType(userLike.getLikeType());
        like.setLikeStatus(userLike.getLikeStatus());
//        like.setLikeCounts(userLike.getLikeCounts());
        like.setCreateTime(TimeUnit.currentTimeFormat());
        like.setUpdateTime(TimeUnit.currentTimeFormat());

        Jedis jedis = RedisConfig.getJedis();
        String likeStr = JSON.toJSONString(like);
        // 点赞数据先存入 redis
        jedis.set("userLikeKey" + "*" +
                TimeUnit.currentTimeFormat().replaceAll(" ", ""), likeStr);

        return CommonResponse.successResponse("感谢点赞");
    }

    /**
     * 点赞同步方法：每10分钟同步redis缓存入库
     *
     * @return
     */
//    @Scheduled(cron = "0 0/10 * * * ?")
    public CommonResponse<String> userLikeRedisCacheTransferToDb() {
        Jedis jedis = RedisConfig.getJedis();
        int resultCount = 0;
        Set<String> userLikeKeysSet = jedis.keys("*" + "userLikeKey" + "*");
        for (String setUserLikeKey : userLikeKeysSet) {
            String userLikeRedisKeyValue = jedis.get(setUserLikeKey);
            UserLike redisCacheUserLike = JSON.parseObject(userLikeRedisKeyValue, UserLike.class);
            int compareResult = userLikeMapper.selectUserLikeDataByUsernameLikeIdAndLikeType(redisCacheUserLike.getUsername(),
                    redisCacheUserLike.getLikeId(),
                    redisCacheUserLike.getLikeType());
            if (compareResult > 0) {
                int userLikeStatusResult =
                        userLikeMapper.selectUserLikeStatusByLikeIdLikeTypeAndUsername(
                                redisCacheUserLike.getUsername(), redisCacheUserLike.getLikeId(),
                                redisCacheUserLike.getLikeType());
                if (userLikeStatusResult == redisCacheUserLike.getLikeStatus()) {
                    return CommonResponse.successResponse();
                } else {
                    resultCount = userLikeMapper.updateUserLikeStatusAndCountsByUsernameLikeIdAndLikeType(redisCacheUserLike);
                }
            } else {
                resultCount = userLikeMapper.insert(redisCacheUserLike);
            }
        }
        if (resultCount > 0) {
            return CommonResponse.successResponse("用户点赞数据同步成功");
        }
        return CommonResponse.errorResponse("系统错误，同步用户点赞数据失败，请联系系统管理员");

    }

    @Override
    public CommonResponse<List<UserLike>> getAllUserLike(HttpSession session) {
        User userLikeUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        QueryWrapper<UserLike> userLikeQueryWrapper = new QueryWrapper<>();
        if (userLikeUser == null) {
            // 查询所有点赞数据
            userLikeQueryWrapper.eq("like_status", 1);
            List<UserLike> userLikeList = userLikeMapper.selectList(userLikeQueryWrapper);
            if (userLikeList.size() > 0) {
                return CommonResponse.successResponse(userLikeList, userLikeList.size());
            }
            return CommonResponse.errorResponse("系统中还没有任何点赞数据哦！~");
        }
        // 查询当前用户
        userLikeQueryWrapper.eq("username", userLikeUser.getUsername());
        userLikeQueryWrapper.eq("like_status", 1);
        List<UserLike> userLikeList = userLikeMapper.selectList(userLikeQueryWrapper);
        if (userLikeList.size() > 0) {
            return CommonResponse.successResponse(userLikeList, userLikeList.size());
        }
        return CommonResponse.errorResponse("系统中还没有任何点赞数据哦！~");
    }


    @Override
    public CommonResponse<String> userCommenting(HttpSession session, @RequestBody UserComment userComment) {
        User loginUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        if (loginUser == null) {
            return CommonResponse.errorResponse("您还没有登录，无法发表评论");
        }
        int resultCount = userCommentMapper.insert(userComment);
        if (resultCount > 0) {
            return CommonResponse.successResponse();
        }
        return CommonResponse.errorResponse("系统错误，评论失败，请联系系统管理员");
    }

    @Override
    public CommonResponse<String> userCommentReply(HttpSession session, @RequestBody UserCommentReply userCommentReply) {
        User loginUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        if (loginUser == null) {
            return CommonResponse.errorResponse("您还没有登录，无法回复评论");
        }
        int resultCount = userCommentReplyMapper.insert(userCommentReply);
        if (resultCount > 0) {
            return CommonResponse.successResponse();
        }
        return CommonResponse.errorResponse("系统错误，回复评论失败，请联系系统管理员");
    }

    @Override
    public CommonResponse<List<UserComment>> getAllUserComment(HttpSession session) {
        User userCommentUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        QueryWrapper<UserComment> userCommentQueryWrapper = new QueryWrapper<>();
        if (userCommentUser == null) {
            // 查询所有评论数据
            List<UserComment> userCommentList = userCommentMapper.selectList(null);
            if (userCommentList.size() > 0) {
                return CommonResponse.successResponse(userCommentList, userCommentList.size());
            }
            return CommonResponse.errorResponse("系统中还没有任何评论哦！~");
        }
        // 查询当前登录用户评论数据
        userCommentQueryWrapper.eq("username", userCommentUser.getUsername());
        List<UserComment> userCommentList = userCommentMapper.selectList(userCommentQueryWrapper);
        if (userCommentList.size() > 0) {
            return CommonResponse.successResponse(userCommentList, userCommentList.size());
        }
        return CommonResponse.errorResponse("系统中还没有任何评论哦！~");
    }

    @Override
    public CommonResponse<String> userFavorite(HttpSession session, @RequestBody UserFavorite userFavorite) {
        User loginUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        if (loginUser == null) {
            return CommonResponse.errorResponse("您还没有登录，无法添加收藏哦！~");
        }
        int resultCount = userFavoriteMapper.insert(userFavorite);
        if (resultCount > 0) {
            return CommonResponse.successResponse("添加收藏成功");
        }
        return CommonResponse.errorResponse("系统错误，添加收藏失败，请联系系统管理员");
    }

    @Override
    public CommonResponse<List<UserFavorite>> getUserFavoriteList(HttpSession session) {
        User loginUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        QueryWrapper<UserFavorite> userFavoriteQueryWrapper = new QueryWrapper<>();
        if (loginUser == null) {
            return CommonResponse.errorResponse("您还没有登录，无法添加收藏哦！~");
        }
        userFavoriteQueryWrapper.eq("username", loginUser.getUsername());
        List<UserFavorite> userFavoriteList = userFavoriteMapper.selectList(userFavoriteQueryWrapper);
        if (userFavoriteList.size() > 0) {
            return CommonResponse.successResponse(userFavoriteList, userFavoriteList.size());
        }
        return CommonResponse.errorResponse("您还没有任何收藏记录哦！~");
    }

    @Override
    public CommonResponse<List<UserVideo>> getUserVideoList(HttpSession session) {
        User loginUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        QueryWrapper<UserVideo> userVideoQueryWrapper = new QueryWrapper<>();
        if (loginUser == null) {
            return CommonResponse.errorResponse("您还没有登录，无法获取视频信息！~");
        }
        userVideoQueryWrapper.eq("username", loginUser.getUsername());
        List<UserVideo> userVideoList = userVideoMapper.selectList(userVideoQueryWrapper);
        if (userVideoList.size() > 0) {
            return CommonResponse.successResponse(userVideoList, userVideoList.size());
        }
        return CommonResponse.errorResponse("您还没有观看任何视频哦！~");
    }

    @Override
    public CommonResponse<List<UserQuestionBank>> getUserQuestionBankList(HttpSession session) {
        User loginUser = (User) session.getAttribute(CommonConst.CURRENT_USER);
        if (loginUser == null){
            return CommonResponse.errorResponse("您还没有登录，无法获取题目信息！~");
        }
        Jedis jedis = RedisConfig.getJedis();
        UserQuestionBank userQuestionBank = null;
        Set<String> tempSet = jedis.smembers("userQuestionBankKey");
        List<UserQuestionBank> userQuestionBankList = new ArrayList<>();
        for (String setTemp : tempSet) {
            userQuestionBank = JSON.parseObject(setTemp, UserQuestionBank.class);
            userQuestionBankList.add(userQuestionBank);
        }
        if (userQuestionBankList.size() > 0) {
            return CommonResponse.successResponse(userQuestionBankList, userQuestionBankList.size());
        }
        return CommonResponse.errorResponse("您还没有收藏任何题目");
    }
}
