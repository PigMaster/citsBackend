package com.steafan.cits.recommend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author steafan
 * @Title: RecommendApplication
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/908:27
 */
@SpringBootApplication
@EnableDiscoveryClient
public class CITSRecommendApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(CITSRecommendApplication.class).run(args);
    }
}
