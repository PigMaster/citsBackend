package com.steafan.cits.service.topic.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@TableName("cits_topic")
public class Topic implements Serializable {
    @TableId
    private int id;
    private String topicName;
    private String topicAuthor;
    private String topicType;
    private String topicDesc;
    private String topicIcon;
    private int topicStatus;
    private String createTime;
    private String updateTime;
}
