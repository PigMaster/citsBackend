package com.steafan.cits.service.topic.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserTopicVO {
    private String topicName;
    private String topicAuthor;
    private String topicType;
    private String topicDesc;
    private int topicStatus;
    private String createTime;
}
