package com.steafan.cits.service.topic.service;

import com.github.pagehelper.PageInfo;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.service.topic.pojo.Topic;
import com.steafan.cits.service.topic.pojo.TopicReply;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface ITopicService {

    CommonResponse<List<Topic>> getAllTopic();

    CommonResponse<Topic> createTopic(String userLoginToken, @RequestBody Topic createTopic);

    CommonResponse<PageInfo> getUserTopic(String userLoginToken, String username, int pageNum, int pageSize);

    CommonResponse<String> userAttendTopic(String userLoginToken, @RequestBody TopicReply topicReply);

    CommonResponse<String> userReplyTopic(String userLoginToke, @RequestBody TopicReply topicReply);

    CommonResponse<String> userCloseTopic(String userLoginToken, @RequestBody Topic topic);

    CommonResponse<List<TopicReply>> getTopicReplyList(int topicId);
}
