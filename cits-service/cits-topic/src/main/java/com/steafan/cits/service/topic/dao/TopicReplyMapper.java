package com.steafan.cits.service.topic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.steafan.cits.service.topic.pojo.TopicReply;
import org.apache.ibatis.annotations.Param;

public interface TopicReplyMapper extends BaseMapper<TopicReply> {

    int updateUserTopicReplyByIdAndUsername(@Param("topicId") int topicId, @Param("username") String username, @Param("topicReply") TopicReply topicReply);
}
