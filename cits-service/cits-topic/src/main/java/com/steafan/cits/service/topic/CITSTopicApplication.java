package com.steafan.cits.service.topic;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * @author steafan
 * @Title: CITSCommentApplication
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/709:59
 */
@SpringBootApplication
@MapperScan("com.steafan.cits.service.topic.dao")
public class CITSTopicApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(CITSTopicApplication.class).run(args);
    }
}
