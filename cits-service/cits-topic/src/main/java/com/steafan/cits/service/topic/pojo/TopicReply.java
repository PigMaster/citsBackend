package com.steafan.cits.service.topic.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@TableName("cits_topic_reply")
public class TopicReply implements Serializable {
    @TableId
    private int id;
    private int topicId;
    private String replyUser;
    private String replyContentDesc;
    private String replyContentImg;
    private String replyTime;
    private String createTime;
    private String updateTime;
}
