package com.steafan.cits.service.topic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.service.topic.dao.TopicMapper;
import com.steafan.cits.service.topic.dao.TopicReplyMapper;
import com.steafan.cits.service.topic.pojo.Topic;
import com.steafan.cits.service.topic.pojo.TopicReply;
import com.steafan.cits.service.topic.service.ITopicService;
import com.steafan.cits.service.topic.vo.UserTopicVO;
import com.steafan.cits.service.user.utils.TokenUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service("topicService")
public class TopicServiceImpl implements ITopicService {

    @Autowired
    private TopicMapper topicMapper;
    @Autowired
    private TopicReplyMapper topicReplyMapper;


    public CommonResponse<List<Topic>> getAllTopic() {
        List<Topic> topicList = topicMapper.selectList(null);
        if (!CollectionUtils.isEmpty(topicList)) {
            return CommonResponse.successResponse(topicList);
        }
        return CommonResponse.errorResponse("系统中还没有任何话题，赶快来创建吧");
    }

    public CommonResponse<Topic> createTopic(String userLoginToken, @RequestBody Topic createTopic) {
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录的token没有传递");
        }
        if (!TokenUtil.verify(userLoginToken)) {
            return CommonResponse.errorResponse("用户未登录，无法创建话题");
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("topic_name", createTopic.getTopicName());
        int topicExistsCount = topicMapper.selectCount(queryWrapper);
        if (topicExistsCount > 0) {
            return CommonResponse.errorResponse("您要创建的话题已存在，请更换名字尝试");
        }
        createTopic.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        createTopic.setUpdateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        int resultCount = topicMapper.insert(createTopic);
        if (resultCount > 0) {
            return CommonResponse.successResponse("创建话题成功", createTopic);
        }
        return CommonResponse.errorResponse("系统错误，创建话题失败");
    }

    public CommonResponse<PageInfo> getUserTopic(String userLoginToken, String username, int pageNum, int pageSize) {
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录的token没有传递");
        }
        if (!TokenUtil.verify(userLoginToken)) {
            return CommonResponse.errorResponse("用户未登录，无法查询话题");
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("topic_author", username);
        PageHelper.startPage(pageNum, pageSize);
        List<Topic> userTopicList = topicMapper.selectList(queryWrapper);
        if (!CollectionUtils.isEmpty(userTopicList)) {
            PageInfo pageResult = new PageInfo(userTopicList);
            pageResult.setList(assembleUserTopicVOList(userTopicList));
            return CommonResponse.successResponse(pageResult);
        }
        return CommonResponse.errorResponse("您还没有创建过任何话题");
    }

    public CommonResponse<String> userAttendTopic(String userLoginToken, @RequestBody TopicReply topicReply){
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录的token没有传递");
        }
        if (!TokenUtil.verify(userLoginToken)) {
            return CommonResponse.errorResponse("用户未登录，无法查询话题");
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("topic_id", topicReply.getTopicId());
        queryWrapper.eq("reply_user", topicReply.getReplyUser());
        int userAttendExistsCount = topicReplyMapper.selectCount(queryWrapper);
        if (userAttendExistsCount > 0) {
            return CommonResponse.errorResponse("您已经加入了该话题，不能重复加入");
        }
        topicReply.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        topicReply.setUpdateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        int resultCount = topicReplyMapper.insert(topicReply);
        if (resultCount > 0) {
            return CommonResponse.successResponse("加入话题成功，现在您可以畅所欲言了");
        }
        return CommonResponse.errorResponse("系统错误，加入话题失败");
    }

    public CommonResponse<String> userReplyTopic(String userLoginToken, @RequestBody TopicReply topicReply){
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录的token没有传递");
        }
        if (!TokenUtil.verify(userLoginToken)) {
            return CommonResponse.errorResponse("用户未登录，无法参与话题讨论");
        }
        // 查询当前用户是否加入了该话题，如果没有加入，则不允许回复
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("reply_user", topicReply.getReplyUser());
        queryWrapper.eq("topic_id", topicReply.getTopicId());
        int replyUserExists = topicReplyMapper.selectCount(queryWrapper);
        if (replyUserExists > 0) {
            // 更新用户回复内容
            int replyResultCount = topicReplyMapper.updateUserTopicReplyByIdAndUsername(topicReply.getTopicId(), topicReply.getReplyUser(), topicReply);
            if (replyResultCount > 0) {
                return CommonResponse.successResponse("话题回复成功");
            }
            return CommonResponse.errorResponse("系统错误，发表话题回复失败");
        }
        return CommonResponse.errorResponse("您没有加入该话题，无法参与讨论");
    }


    public CommonResponse<List<TopicReply>> getTopicReplyList(int topicId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("topic_id", topicId);
        List<TopicReply> topicReplyList = topicReplyMapper.selectList(queryWrapper);
        if (!CollectionUtils.isEmpty(topicReplyList)) {
            return CommonResponse.successResponse(topicReplyList);
        }
        return CommonResponse.errorResponse("该话题下还没有任何回复内容，赶快来发表回复吧");
    }

    public  CommonResponse<String> userCloseTopic(String userLoginToken, @RequestBody Topic topic) {
        if (StringUtils.isEmpty(userLoginToken)) {
            return CommonResponse.errorResponse("用户登录的token没有传递");
        }
        if (!TokenUtil.verify(userLoginToken)) {
            return CommonResponse.errorResponse("用户未登录，无法关闭话题");
        }
        // 校验当前用户是否是话题的创建者
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", topic.getId());
        queryWrapper.eq("topic_author", topic.getTopicAuthor());
        int userResultCount = topicMapper.selectCount(queryWrapper);
        if (userResultCount > 0) {
            int resultCount = topicMapper.updateTopicStatusByTopicAuthorAndId(topic.getId(), topic.getTopicAuthor());
            if (resultCount > 0) {
                return CommonResponse.successResponse("话题关闭成功，话题回复权限关闭，且话题不能被重新开启");
            }
            return CommonResponse.errorResponse("系统错误，关闭话题失败");
        }
        return CommonResponse.errorResponse("您不是该话题的创建者，不能关闭该话题");
    }

    private List<UserTopicVO> assembleUserTopicVOList(List<Topic> userTopicList) {
        List<UserTopicVO> userTopicVOList = Lists.newArrayList();
        for (Topic userTopic : userTopicList) {
            UserTopicVO userTopicVO = new UserTopicVO();
            userTopicVO.setTopicName(userTopic.getTopicName());
            userTopicVO.setTopicAuthor(userTopic.getTopicAuthor());
            userTopicVO.setTopicType(userTopic.getTopicType());
            userTopicVO.setTopicDesc(userTopic.getTopicDesc());
            userTopicVO.setTopicStatus(userTopic.getTopicStatus());
            userTopicVO.setCreateTime(userTopic.getCreateTime());
            userTopicVOList.add(userTopicVO);
        }
        return userTopicVOList;
    }
}
