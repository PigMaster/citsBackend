package com.steafan.cits.service.topic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.steafan.cits.service.topic.pojo.Topic;
import org.apache.ibatis.annotations.Param;

public interface TopicMapper extends BaseMapper<Topic> {

    int updateTopicStatusByTopicAuthorAndId(@Param("id") int id, @Param("username") String username);
}
