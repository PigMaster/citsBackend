package com.steafan.cits.service.topic.controller;

import com.github.pagehelper.PageInfo;
import com.steafan.cits.common.commons.CommonResponse;
import com.steafan.cits.service.topic.pojo.Topic;
import com.steafan.cits.service.topic.pojo.TopicReply;
import com.steafan.cits.service.topic.service.ITopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/topic/")
public class TopicController {


    @Autowired
    private ITopicService topicService;

    /**
     * 获取所有已创建话题列表
     *
     * @return
     */
    @RequestMapping(value = "get_all_topic.do", method = RequestMethod.GET)
    @ResponseBody
    public CommonResponse<List<Topic>> getAllTopic(){
        return topicService.getAllTopic();
    }

    /**
     * 用户创建话题
     *
     * @param userLoginToken
     * @param createTopic
     * @return
     */
    @RequestMapping(value = "create_topic.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<Topic> createTopic(String userLoginToken, @RequestBody Topic createTopic){
        return topicService.createTopic(userLoginToken, createTopic);
    }

    /**
     * 获取用户创建的话题列表
     *
     * @param userLoginToken
     * @param username
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "get_user_topic.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<PageInfo> getUserTopic(String userLoginToken, String username, int pageNum, int pageSize){
        return topicService.getUserTopic(userLoginToken, username, pageNum, pageSize);
    }

    /**
     * 用户加入话题，录入用户名、话题ID
     *
     * @param userLoginToken
     * @param topicReply
     * @return
     */
    @RequestMapping(value = "user_attend_topic.do", method = RequestMethod.POST)
    @ResponseBody
    public  CommonResponse<String> userAttendTopic(String userLoginToken, @RequestBody TopicReply topicReply){
        return topicService.userAttendTopic(userLoginToken, topicReply);
    }

    /**
     * 用户回复话题
     *
     * @param userLoginToken
     * @param topicReply
     * @return
     */
    @RequestMapping(value = "user_reply_topic.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<String> userReplyTopic(String userLoginToken, @RequestBody TopicReply topicReply){
        return topicService.userReplyTopic(userLoginToken, topicReply);
    }

    /**
     * 获取具体话题下所有用户的回复内容
     *
     * @param topicId
     * @return
     */
    @RequestMapping(value = "get_topic_reply_list.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<List<TopicReply>> getTopicReplyList(int topicId){
        return topicService.getTopicReplyList(topicId);
    }

    /**
     * 用户关闭话题，关闭话题之后，话题回复权限也将关闭，且话题不能被重新打开
     *
     * @param userLoginToken
     * @param topic
     * @return
     */
    @RequestMapping(value = "user_close_topic.do", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<String> userCloseTopic(String userLoginToken, @RequestBody Topic topic){
        return topicService.userCloseTopic(userLoginToken, topic);
    }
}
