# citsBackend

#### 介绍
CITS持续集成统一学习测评系统是一款基于SpringCloud的在线学习测评系统，详细功能见下图，包括但不限于IT技术知识、医疗领域知识等。目前该项目处于开发阶段，欢迎感兴趣的Star，谢谢！
![CITS持续集成统一学习测评系统功能架构设计](https://images.gitee.com/uploads/images/2020/0407/104759_d073c8e4_1440851.png "屏幕截图.png")

#### 系统架构

微服务治理核心：Spring Cloud Alibaba
服务注册发现中心：Nacos 2.0.2.RELEASE
数据库ORM处理：MyBatis-Plus 3.2.0
数据库：MySQL 5.7

#### 重大变更记录

2020.4.20 项目架构重构，项目微服务治理核心框架逐步改用Spring Cloud Alibaba，去除Eureka服务注册发现中心，替换为Nacos服务注册发现中心，Zuul网关做相应调整。
Nacos管控台截图：
![Nacos管控台](https://images.gitee.com/uploads/images/2021/0420/100847_f6860563_1440851.png "屏幕截图.png")

#### 安装教程

目前该项目处于开发期，暂不提供


#### 使用说明

目前该项目处于开发期，暂不提供


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
