package com.steafan.cits.gateway;

import org.apache.catalina.filters.CorsFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * @author steafan
 * @Title: CITSGatewayApplication
 * @ProjectName cits.pro
 * @Description: TODO
 * @date 2020/4/709:50
 */
@SpringCloudApplication
@EnableZuulProxy
public class CITSGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(CITSGatewayApplication.class, args);
    }

}
